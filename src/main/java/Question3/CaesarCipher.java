package Question3;

public class CaesarCipher extends SubstitutionCipher {

    public CaesarCipher(int numberOfRotations, int gapSize) {
        super(numberOfRotations, gapSize);
    }

    @Override
    protected String generateCipherResult(String textToCipher, int numberOfRotations){
        StringBuilder encryptedText = new StringBuilder();
        for (char letter : textToCipher.toCharArray()) {
            encryptedText.append(letterRotator.rotateLetter(letter,numberOfRotations));
        }
        return encryptedText.toString();
    }

    //Ignore this
    @Override
    protected char generateCipherLetter(char letterToCipher, int replaceLetter, int numberOfRotations) {
        return ' ';
    }
}
