package Question1;

public interface Cipher{
    String encrypt(String textToEncrypt);
    String decrypt(String textToDecrypt);
    String generateCipherResult(String textToCipher, int numberOfRotations);
    char generateCipherLetter(char letterToCipher, int replaceLetter, int numberOfRotations);
}
