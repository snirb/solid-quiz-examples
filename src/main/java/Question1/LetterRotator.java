package Question1;


public class LetterRotator {

    private static final int LOWER_CASE_A = 65;
    private static final int UPPER_CASE_A = 97;

    public char rotateLetter(char letter, int numberOfRotations) {
        int asciiFirstLetter;
        asciiFirstLetter = getAsciiFirstLetter(letter);
        return (char) ((letter + numberOfRotations - asciiFirstLetter) % 26 + asciiFirstLetter);
    }

    private int getAsciiFirstLetter(char letter) {
        if (Character.isUpperCase(letter))
            return LOWER_CASE_A;
        else
            return UPPER_CASE_A;
    }

}
